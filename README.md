# Dextractor

Dextractor is a CLI tool written with NodeJS to extract links from within a given URL, save them inside a text file and download the files from the links that actually end to a file.

**Note:** This app uses the package [Dextractor](https://www.npmjs.com/package/dextractor "Dextractor on npm").

## Installation

I assume that you have [NodeJS](https://nodejs.org) already installed on your machine.

**1.** Clone the project on your local machine or server:

```bash
git clone https://gitlab.com/kyxey/dextractor.git
```

**2.** Simply ``cd`` to the root of the project and run:

```bash
npm install
```

Done!

---

## Usage

First you need to ``cd`` into the root of the project and run:

```bash
npm run start
```

After that, you'll see this screen:
![Screenshot 1 of Dextractor](https://monaxon.com/Dextractor/1.png)
You should choose your mode.

**Mode 0** [default mode], **mode 1** or **mode 2**.

**Mode 0** which is the default mode, will extract and save all of the links from within a given URL in a text file in ``./export/[Given URL separated with underlines instead of forward slashes]/links/links.txt`` and then will start downloading the files from the links that actually end to a file and will compress the downloaded files and save them in ``./export/[Given URL separated with underlines instead of forward slashes]/files.zip`` so you can use them later.

**Mode 1** which you have to type it in order to be selected, will only extract and save all of the links from within a given URL in a text file in ``./export/[Given URL separated with underlines instead of forward slashes]/links/links.txt``.

**Mode 2** which you have to type it in order to be selected, will only download the files from the links that actually end to a file and will compress the downloaded files and save them in ``./export/[Given URL separated with underlines instead of forward slashes]/files.zip`` so you can use them later.

**Note 1:** The downloading feature only works on the direct and static downloading links e.g. ``https://example.com/image.png`` meaning that dynamic download links will not work.

**Note 2:**: The link extraction feature works properly only on static websites or static file servers. On a dynamic website you might get links to the personal files of the website.

After selecting your mode, you'll be prompted to enter your URL:
![Screenshot 2 of Dextractor](https://monaxon.com/Dextractor/2.png)
If you enter a valid URL, the app will start doing its job after hitting enter. If you enter an invalid URL, the app will throw an error and will exit. If you enter ``exit`` when prompted for URL, the app will exit.
The app will give you its status as a log in every step. It will quit after doing its job.

You can move your downloaded links and files anywhere and use them after they got downloaded.

---

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

---

## License

[MIT](https://choosealicense.com/licenses/mit/)
