import dextractor from "dextractor";

function Actions() {
  //----Public Methods:----
  this.start = function(link, mode, callback = () => {}) {
    switch (mode) {
      case 0: {
        dextractor.saveLinksAndDownloadFiles(link, "./export/", callback);
        break;
      }
      case 1: {
        dextractor.saveLinks(link, "./export/", callback);
        break;
      }
      case 2: {
        dextractor.downloadFiles(link, "./export/", callback);
        break;
      }
      default: {
        break;
      }
    }
  };
  //----Public Methods End.----
}
const instanceOfActions = new Actions();
Object.freeze(instanceOfActions);
export default instanceOfActions;
