import readline from "readline";
import instanceOfActions from "./actions/Actions.js";

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

console.clear();
console.log("-------Welcome to Dextractor!-------\n");
console.log(
  "******Dextractor can save all the links from a given URL in a text file and download all of the links that actually end to a file.******\n"
);

rl.question(
  "Please select your mode\n(0[default]: Save links and download files.\n1: Only save links.\n2: Only download files.): ",
  mode => {
    rl.question("Please enter your URL: ", function(url) {
      if (url === "exit") {
        rl.close();
      } else {
        instanceOfActions.start(
          url,
          mode === "1" ? 1 : mode === "2" ? 2 : 0,
          () => {
            console.log("Done!");
            rl.close();
          }
        );
      }
    });
  }
);

rl.on("close", function() {
  console.log("\nGoodbye.");
  process.exit(0);
});
